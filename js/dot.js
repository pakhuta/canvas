class Dot {
    constructor(positions) {
        this.positions = {...positions};

        this.speed = {
            dX: this.getRandomSpeed(config.dot.maxSpeed),
            dY: this.getRandomSpeed(config.dot.maxSpeed)
        };

        this.radius = config.dot.radius;
    }

    move() {
        this.positions.x += this.speed.dX;
        this.positions.y += this.speed.dY;
    }

    getRandomSpeed(value) {
        return Utils.getRandomInteger(-value, value);
    }
}