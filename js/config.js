const config = {
    dot: {
        color: '#c2c2c2',
        minCount: 10,
        maxSpeed: 4,
        radius: 3,
        dotsByClick: 100
    },
    line: {
        opacity: {
            strong: 1,
            weak: 0.2
        },
        maxLength: 100
    },
    updateInterval: 50,
    removeInterval: 500
};