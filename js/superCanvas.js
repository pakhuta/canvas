class SuperCanvas {
    constructor(canvasElement) {
        this.ctx = canvasElement.getContext('2d');
        this.darawParams = {};
    }

    drawDot(dot) {
        this.ctx.fillStyle = this.darawParams.fillStyle;
        this.ctx.globalAlpha = this.darawParams.globalAlpha;
        this.ctx.beginPath();
        this.ctx.arc(dot.positions.x, dot.positions.y, dot.radius, 0, 2 * Math.PI);
        this.ctx.fill();
    }

    drawLine(dot1, dot2) {
        this.ctx.strokeStyle = this.darawParams.strokeStyle;
        this.ctx.globalAlpha = this.darawParams.globalAlpha;
        this.ctx.beginPath();
        this.ctx.moveTo(dot1.positions.x, dot1.positions.y);
        this.ctx.lineTo(dot2.positions.x, dot2.positions.y);
        this.ctx.stroke();
    }

    setDrawParams(params) {
        let defaultParams = {
            strokeStyle: config.dot.color,
            fillStyle: config.dot.color,
            globalAlpha: 1
        };

        this.darawParams = Object.assign(defaultParams, params);

    }

    clear() {
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    }

    get canvasElement() {
        return this.ctx.canvas;
    }
}