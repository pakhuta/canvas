class App {
    constructor(canvas) {
        this.dots = [];
        this.superCanvas = new SuperCanvas(canvas);
        this.setCanvasSize();

        this.addDotsToMin();

        document.addEventListener('click', (event) => this.addDots({x: event.clientX, y: event.clientY}));
        window.addEventListener('resize', () => this.setCanvasSize());

        setInterval(() => this.updateCanvas(), config.updateInterval);
        setInterval(() => this.removeRedundantDots(), config.removeInterval);
    }

    removeRedundantDots() {
        this.dots = this.dots.filter(dot => this.isVisible(dot));
        this.addDotsToMin();
    }

    addDotsToMin() {
        let countToAdd = config.dot.minCount - this.dots.length;

        if (countToAdd > 0) {
            [...Array(countToAdd)].forEach(() => this.addDot());
        }
    }

    isVisible(dot) {
        let maxX = document.documentElement.clientWidth;
        let maxY = document.documentElement.clientHeight;
        let isXVisible = (dot.positions.x) > 0 && (dot.positions.x < maxX);
        let isYVisible = (dot.positions.y) > 0 && (dot.positions.y < maxY);

        return isXVisible && isYVisible;
    }

    addDot(positions = this.getRandomPositions()) {
        this.dots.push(new Dot(positions));
    }

    getRandomPositions() {
        return {
            x: Utils.getRandomInteger(0, this.superCanvas.canvasElement.width),
            y: Utils.getRandomInteger(0, this.superCanvas.canvasElement.height)
        };
    }

    addDots(positions) {
        [...Array(config.dot.dotsByClick)].forEach(() => this.addDot(positions));
    }

    updateCanvas() {
        this.superCanvas.clear();

        this.drawDots();
        this.drawLines();
    }

    drawDots() {
        this.superCanvas.setDrawParams();
        this.dots.forEach(dot => {
            dot.move();
            this.superCanvas.drawDot(dot);
        });
    }

    drawLines() {
        this.dots.forEach((dot1, index) => {
            for (let i = index + 1; i < this.dots.length; i++) {
                let dot2 = this.dots[i];
                let lenght = this.getLehgth(dot1, dot2);

                if (lenght < config.line.maxLength) {
                    this.superCanvas.setDrawParams({globalAlpha: this.getOpacity(lenght)});
                    this.superCanvas.drawLine(dot1, dot2)
                }
            }
        });
    }

    getLehgth(dot1, dot2) {
        let sideX = dot1.positions.x - dot2.positions.x;
        let sideY = dot1.positions.y - dot2.positions.y;
        return Math.hypot(sideX, sideY);
    }

    setCanvasSize() {
        this.superCanvas.canvasElement.width = document.documentElement.clientWidth;
        this.superCanvas.canvasElement.height = document.documentElement.clientHeight;
    }

    getOpacity(length) {
        return config.line.opacity.strong + (config.line.opacity.weak - config.line.opacity.strong) * length / config.line.maxLength;
    }
}

let app = new App(document.querySelector('.canvas'));